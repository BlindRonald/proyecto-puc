import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectLeasingComponent } from './select-leasing.component';

describe('SelectLeasingComponent', () => {
  let component: SelectLeasingComponent;
  let fixture: ComponentFixture<SelectLeasingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectLeasingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectLeasingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
