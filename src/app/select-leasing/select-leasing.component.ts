import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'app-select-leasing',
  templateUrl: './select-leasing.component.html',
  styleUrls: ['./select-leasing.component.css']
})
export class SelectLeasingComponent implements OnInit {

  perfiles = ["PERFIL 001", "PERFIL 002", "PERFIL 003", "PERFIL 004"];

  constructor(private router: Router) {

  }

  ngOnInit() {
  }

  perfilClick(skill: any) {
    this.router.navigate(['productos']);
  }

}
