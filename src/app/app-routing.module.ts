import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomeComponent } from './welcome/welcome.component';
import { SelectLeasingComponent } from './select-leasing/select-leasing.component';

import { ProductsComponent } from './products/products.component';


const routes: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'leasing-home', component: SelectLeasingComponent },
  { path: 'productos', component: ProductsComponent },
  // ProductsComponent
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
