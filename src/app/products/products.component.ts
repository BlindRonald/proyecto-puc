import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  nombre_cliente: string;
  solicitud: string;
  rut_cliente: string;
  f_ingreso: string;
  monto: number;
  etapa: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {solicitud: '000234378', nombre_cliente: 'Distribuidora DISAC S.A', rut_cliente: '99.575.908-3', f_ingreso: '21 Dic 2019', monto: 300, etapa: "Oferta"},
  {solicitud: '000274371', nombre_cliente: 'Distribuidora DISAC S.A', rut_cliente: '99.575.908-3', f_ingreso: '21 Jun 2019', monto: 15.000, etapa: "Cursado"},
  {solicitud: '000274288', nombre_cliente: 'Distribuidora DISAC S.A', rut_cliente: '99.575.908-3', f_ingreso: '21 Nov 2019', monto: 15.000, etapa: "Oferta"},
  {solicitud: '002654889', nombre_cliente: 'Distribuidora DISAC S.A', rut_cliente: '99.575.908-3', f_ingreso: '21 Jul 2019', monto: 1000, etapa: "Control"},
  {solicitud: '000265538', nombre_cliente: 'Distribuidora DISAC S.A', rut_cliente: '99.575.908-3', f_ingreso: '21 Ene 2019', monto: 3000, etapa: "Cursado"},
];

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  displayedColumns: string[] = ['solicitud', 'nombre_cliente', 'rut_cliente', 'f_ingreso', 'monto', 'etapa'];
  dataSource = ELEMENT_DATA;

  constructor() {

  }

  ngOnInit() {
  }

}
